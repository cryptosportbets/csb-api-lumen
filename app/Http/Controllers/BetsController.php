    <?php

namespace App\Http\Controllers;

use App\Bets;
use Illuminate\Http\Request;

class BetsController extends Controller
{

    public function showAllAuthors()
    {
        return response()->json(Bets::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(Bets::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'idPool' => 'required',
            'idLeagues' => 'required',
            'idMatches' => 'required'
        ]);

        $cryptos = Bets::create($request->all());

        return response()->json($cryptos, 201);
    }

    public function update($id, Request $request)
    {
        $cryptos = Bets::findOrFail($id);
        $cryptos->update($request->all());

        return response()->json($cryptos, 200);
    }

    public function delete($id)
    {
        Bets::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}