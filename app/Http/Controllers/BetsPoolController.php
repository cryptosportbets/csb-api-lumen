    <?php

namespace App\Http\Controllers;

use App\BetsPool;
use Illuminate\Http\Request;

class BetsPoolController extends Controller
{

    public function showAllAuthors()
    {
        return response()->json(BetsPool::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(BetsPool::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'utcDateTime' => 'required',
            'totalBets' => 'required'
        ]);

        $cryptos = BetsPool::create($request->all());

        return response()->json($cryptos, 201);
    }

    public function update($id, Request $request)
    {
        $cryptos = BetsPool::findOrFail($id);
        $cryptos->update($request->all());

        return response()->json($cryptos, 200);
    }

    public function delete($id)
    {
        BetsPool::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}