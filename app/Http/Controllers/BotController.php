<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class BotController extends BaseController
{

    public function home()
    {
        //do our login mechanisms here
        return view("index",['name' => 'James']);
    }
}
