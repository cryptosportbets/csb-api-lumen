<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *    title="Your super  ApplicationAPI",
 *    version="1.0.0",
 * )
 * @OA\PathItem(
 * 	  path="MyPath"
 * )
 * @SWG\Info(
 *    title="Swagger 3.0",
 *    version="3.0"
 *)
 */

class Controller extends BaseController
{
    //
}
