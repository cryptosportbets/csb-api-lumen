    <?php

namespace App\Http\Controllers;

use App\Cryptos;
use Illuminate\Http\Request;

class CryptosController extends Controller
{

    public function showAllAuthors()
    {
        return response()->json(Cryptos::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(Cryptos::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $cryptos = Cryptos::create($request->all());

        return response()->json($cryptos, 201);
    }

    public function update($id, Request $request)
    {
        $cryptos = Cryptos::findOrFail($id);
        $cryptos->update($request->all());

        return response()->json($cryptos, 200);
    }

    public function delete($id)
    {
        Cryptos::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}