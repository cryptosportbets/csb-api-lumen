<?php
namespace App\Http\Controllers;

use App\Leagues;
use Illuminate\Http\Request;

class LeaguesController extends Controller
{

    public function showAllAuthors()
    {
        return response()->json(Leagues::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(Leagues::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'creationUtcDateTime' => 'required',
            'cryptoShortName' => 'required',
            'matchesList' => 'required'
        ]);

        $cryptos = Leagues::create($request->all());

        return response()->json($cryptos, 201);
    }

    public function update($id, Request $request)
    {
        $cryptos = Leagues::findOrFail($id);
        $cryptos->update($request->all());

        return response()->json($cryptos, 200);
    }

    public function delete($id)
    {
        Leagues::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}