<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class LogController extends BaseController
{

    public function home()
    {
        //do our login mechanisms here
    	$content = null;

		$fileArray = $this->getMyFileContentAsArray(storage_path().'/logs/your.log','');
		//$fileArray = array_reverse($fileArray);

		foreach ($fileArray as $key => $line) {
		  $content .= $line.'<br/>';
		}

        return view("logs",array('content' => $content));
    }

    public function lumen()
    {
        //do our login mechanisms here
    	$content = null;
    	
		$fileArray = $this->getMyFileContentAsArray(storage_path().'/logs/lumen-'.date('Y-m-d').'.log','');
		//$fileArray = array_reverse($fileArray);

		foreach ($fileArray as $key => $line) {
		  $content .= $line.'<br/>';
		}

        return view("logs",array('content' => $content));
    }    

	/**
	 * Open a file and return array of line
	 *
	 * @access public
	 * @param 		string 	$url 		url of file
	 * @param 		string 	$filter 	filter lines returned
	 * @return 		array 	$lines
	 */
	function getMyFileContentAsArray($url,$filter){
		$file = fopen($url, "r");
		$lines = array();

		while (!feof($file)) {
			$line = fgets($file);
			if($filter != null){				
				if(strpos($line,$filter) !== false){
					$lines[] = $line;
				}
			}else{
				$lines[] = $line;
			}
		}

		fclose($file);	
		return $lines;	
	}	

}
