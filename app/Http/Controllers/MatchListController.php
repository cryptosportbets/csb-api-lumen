<?php

namespace App\Http\Controllers;
use App\Leagues;
use App\Matches;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Laravel\Lumen\Routing\Controller as BaseController;

class MatchListController extends BaseController
{

    public function display($cryptoShortName,$sessionId)
    {
		$log = new Logger('');
		$log->pushHandler(new StreamHandler(storage_path().'/logs/your.log', Logger::INFO));


		$log->info('cryptoShortName : '.$cryptoShortName);

		// create a log channel

        $values = Leagues::where('cryptoShortName', $cryptoShortName)->take(1)->get();

$log->info('matches : '.json_encode($values));

        // Match List
        $matchesList = $values[0]['matchesList'];
        //$log->info('matchesList : '.json_encode($matchesList));
        $matchesTmp = explode('-', $matchesList);
        $matchesArray = array();
        foreach ($matchesTmp as $key => $value) {
            $match = Matches::where('idProvider', $value)->take(1)->get();
            $matchesArray[] = $match;
            $log->info('K is : '.$key);
            $log->info('V is : '.$value);
            $log->info('Match is : '.json_encode($match));
        }


        //do our login mechanisms here
        return view("matches",['matchesArray' => $matchesArray, 'sessionId' => $sessionId, 'cryptoShortName' => $cryptoShortName]);
    }
}