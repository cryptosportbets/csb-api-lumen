<?php
namespace App\Http\Controllers;

use App\Matches;
use Illuminate\Http\Request;

class MatchesController extends Controller
{

    public function showAllAuthors()
    {
        return response()->json(Matches::all());
    }

    public function showOneAuthor($id)
    {
        return response()->json(Matches::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'idProvider' => 'required',
            'utcDateTime' => 'required',
            'homeTeam' => 'required',
            'awayTeam' => 'required',
        ]);

        $cryptos = Matches::create($request->all());

        return response()->json($cryptos, 201);
    }

    public function update($id, Request $request)
    {
        $cryptos = Matches::findOrFail($id);
        $cryptos->update($request->all());

        return response()->json($cryptos, 200);
    }

    public function delete($id)
    {
        Matches::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}