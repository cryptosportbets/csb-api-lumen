<?php

namespace App\Http\Controllers;

use App\Cryptos;
use App\Leagues;
use App\Matches;
use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class PrismeController extends Controller
{

    public function create(Request $request)
    {
        /*$this->validate($request, [
            'intent' => 'required', 'value' => 'required'
        ]);
        $intent = $request['intent'];
        $value = $request['value'];*/
        //echo json_encode($request);
                
// create a log channel
$log = new Logger('');
$log->pushHandler(new StreamHandler(storage_path().'/logs/your.log', Logger::INFO));

// add records to the log
/*$log->info('checking request');
$log->info($request);
$log->info($request['event']);
$log->info($request['sessionId']);*/
$event = $request['event'];
$sessionId = $request['sessionId'];
$log->info($event.' & '.$sessionId);
/*$log->info($request['user']);
$log->info($request['query']);
$log->info(json_encode($request['intents']));*/

        $settings = $this->choices($event,$sessionId,$log);
        $streams = $this->formatStream($settings,$log);
        //$log->info(json_encode($settings));

        return response()->json($streams, 201);
    }

    private function choices($event,$sessionId,$log)
    {
        $values = array();
        $outputFormat = array();
        $settings = array('values' => null, 'outputFormat' => null);
        switch ($event) {
        	case 'WELCOME':
        		$values = Cryptos::all();
                $log->info('Values are & '.json_encode($values));
                /*$values = array();
                $element = array('id' => 2, 'name' => 'Etherum', 'imgUrl' => 'https://csb-api-lumen.herokuapp.com/img/etherum.png');
                $values[] = $element;
                $element = array('id' => 1, 'name' => 'Bitcoin', 'imgUrl' => 'https://csb-api-lumen.herokuapp.com/img/Bitcoin.png');
                $values[] = $element;   */             
                $contentArray = array('title' => 'name', 'text' => 'name', 'image' => 'imgUrl');
                $actions = array();
                $actions[] = array('text' => 'Parier', 'value' => 'shortName', 'type' => 'event');
                $actionsArray = $actions;
                $outputFormat = array('type' => 'card', 'associations' => $contentArray, 'actionsArray' => $actionsArray);
        		break;
        	
            case "BTC":
            case "ETH":
                $values = Leagues::where('cryptoShortName', $event)->take(1)->get();

                // Match List
                $matchesList = $values[0]['matchesList'];
                $matchesTmp = explode('-', $matchesList);
                $matchesArray = array();
                foreach ($matchesTmp as $key => $value) {
                    $match = Matches::where('idProvider', $value)->take(1)->get();
                    $matchesArray[] = $match;
                    $log->info('K is : '.$key);
                    $log->info('V is : '.$value);
                    $log->info('Match is : '.json_encode($match));
                }
                //$values = Leagues::find(1);
                //$values = Leagues::all();
                $log->info('Values are & '.json_encode($values));

                $outputFormat = array('type' => 'text', 'value' => json_encode($matchesList));                
                break;
        	default:

        		break;
        }

        return $settings = array('values' => $values, 'outputFormat' => $outputFormat);
    }

    private function formatStream($settings,$log){
        $stream = null;
    	$values = $settings['values'];
        $outputFormat = $settings['outputFormat'];

        if($values != null){
            $type = $outputFormat['type'];

            switch ($type) {
                case 'card':
                    // Expected 
                    // {"type" : "card","title":"I am a card","image":"image URL","text":"some text..","buttons":
                    // [
                    //      {"type":"button | link","text":"...","value":"..."},
                    //      {"..."}
                    // ]}
                    
                    $items = array();
                    foreach ($values as $keyResult => $valueResult) {
                        //$log->info('keyResult => '.$keyResult);
                        //$log->info('valueResult => '.$valueResult);
                        
                        $card = array();

                        $associations = $outputFormat['associations'];
                        foreach ($associations as $keyAssociation => $valueAssociation) {
                            //$log->info('keyAssociation => '.$keyAssociation);
                            //$log->info('valueAssociation => '.$valueAssociation);
                            
                            $card['type'] = $type;
                            $card[$keyAssociation] = $valueResult[$valueAssociation];
                        }
                        
                        $actions = $outputFormat['actionsArray'];
                        $actionsItems = array();
                        $actionsTmp = array();
                        foreach ($actions as $keyAction => $valueAction) {
                            $a = $valueAction;
                            foreach ($a as $k => $v) {
                                $log->info('k => '.$k);
                                $log->info('v => '.$v);
                                
                                if($k == 'value') 
                                    $actionsItems[$k] = $valueResult[$v];
                                else
                                    $actionsItems[$k] = $v;
                            }
                            $actionsTmp[] = $actionsItems;
                            $actionsItems = array();
                        }

                        $card['buttons'] = $actionsTmp;
                        //$card['events2'] = $actionsItems;

                        $items[] = $card;
                    }

                    $stream = array('stream' => $items);                  

                    break;
                case 'text':
                    $total = $outputFormat['value'];
                    $items = array();
                    $items[] = array('text' => $total);
                    $stream = array('stream' => $items);
                break;
                default:
                    //
                    break;
            }
        }

    	return $stream;
    }

}