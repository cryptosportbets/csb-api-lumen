<?php
namespace App\Http\Controllers;

use App\Teams;
use Illuminate\Http\Request;

class TeamsController extends Controller
{

    public function showAllTeams()
    {
        return response()->json(Teams::all());
    }

    public function showOneTeams($id)
    {
        return response()->json(Teams::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'shortName' => 'required'
        ]);

        $cryptos = Teams::create($request->all());

        return response()->json($cryptos, 201);
    }

    public function update($id, Request $request)
    {
        $cryptos = Teams::findOrFail($id);
        $cryptos->update($request->all());

        return response()->json($cryptos, 200);
    }

    public function delete($id)
    {
        Teams::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}