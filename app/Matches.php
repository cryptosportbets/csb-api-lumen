<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idProvider', 'utcDateTime', 'homeTeam', 'homeTeamImgUrl', 'awayTeam', 'awayTeamImgUrl', 'winner'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}