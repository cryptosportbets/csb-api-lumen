<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idProvider', 'shortName', 'imgUrl', 'isPremium'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}