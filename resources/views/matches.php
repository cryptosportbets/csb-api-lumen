<!DOCTYPE html>
<html>
<head>
	<title>Crypto <?php echo $cryptoShortName ?></title>
</head>
<body>
<form>

	<table width="100%" style="text-align: center">
		<tr>
			<th></th>
			<th></th>
			<th>Team 1</th>
			<th>Bet</th>
			<th>Team 2</th>
			<th></th>
		</tr>
		<?php
		foreach ($matchesArray as $key => $value) {
			$match = json_decode($value[0],true);
			
			echo '<tr>';
				echo '<td>';
				echo $match['idProvider']; 
				echo '</td>';
				echo '<td>';
				echo '<img src="'.$match['homeTeamImgUrl'].'" height="30px">'; 
				echo '</td>';
				echo '<td>';
				echo $match['homeTeam']; 
				echo '</td>';
				echo '<td>';
				echo 'Bet'; 
				echo '</td>';
				echo '<td>';
				echo $match['awayTeam']; 
				echo '<//td>';	
				echo '<td>';
				echo '<img src="'.$match['awayTeamImgUrl'].'" height="30px">'; 
				echo '<//td>';												
			echo '<tr>';
		}
		?>
	</table>

	<input type="hidden" name="sessionId" value="<?php echo $sessionId ?>">
</form>
</body>
</html>
