<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*$router->get('/', function () use ($router) {
    return $router->app->version();
});*/

$router->group(['prefix' => 'api'], function () use ($router) {

  $router->get('cryptos',  ['uses' => 'CryptosController@showAllCryptos']);

  $router->get('cryptos/{id}', ['uses' => 'CryptosController@showOneCryptos']);

  $router->post('cryptos', ['uses' => 'CryptosController@create']);

  $router->delete('cryptos/{id}', ['uses' => 'CryptosController@delete']);

  $router->put('cryptos/{id}', ['uses' => 'CryptosController@update']);

  $router->get('teams',  ['uses' => 'TeamsController@showAllTeams']);

  $router->get('teams/{id}', ['uses' => 'TeamsController@showOneTeams']);

  $router->post('teams', ['uses' => 'TeamsController@create']);

  $router->delete('teams/{id}', ['uses' => 'TeamsController@delete']);

  $router->put('teams/{id}', ['uses' => 'TeamsController@update']);

  $router->get('matches',  ['uses' => 'MatchesController@showAllMatches']);

  $router->get('matches/{id}', ['uses' => 'MatchesController@showOneMatches']);

  $router->post('matches', ['uses' => 'MatchesController@create']);

  $router->delete('matches/{id}', ['uses' => 'MatchesController@delete']);

  $router->put('matches/{id}', ['uses' => 'MatchesController@update']);

  $router->get('leagues',  ['uses' => 'LeaguesController@showAllLeagues']);

  $router->get('leagues/{id}', ['uses' => 'LeaguesController@showOneLeagues']);

  $router->post('leagues', ['uses' => 'LeaguesController@create']);

  $router->delete('leagues/{id}', ['uses' => 'LeaguesController@delete']);

  $router->put('leagues/{id}', ['uses' => 'LeaguesController@update']);

  $router->get('bets',  ['uses' => 'BetsController@showAllBets']);

  $router->get('bets/{id}', ['uses' => 'BetsController@showOneBets']);

  $router->post('bets', ['uses' => 'BetsController@create']);

  $router->delete('bets/{id}', ['uses' => 'BetsController@delete']);

  $router->put('bets/{id}', ['uses' => 'BetsController@update']);

  $router->get('betsPool',  ['uses' => 'BetsPoolController@showAllBetsPool']);

  $router->get('betsPool/{id}', ['uses' => 'BetsPoolController@showOneBetsPool']);

  $router->post('betsPool', ['uses' => 'BetsPoolController@create']);

  $router->delete('betsPool/{id}', ['uses' => 'BetsPoolController@delete']);

  $router->put('betsPool/{id}', ['uses' => 'BetsPoolController@update']);    

});

$router->group(['prefix' => 'api'], function () use ($router) {

  $router->post('prisme', ['uses' => 'PrismeController@create']);

});

$router->get('/', ['uses' => 'BotController@home']);
$router->get('/logs', ['uses' => 'LogController@home']);
$router->get('/lumen-logs', ['uses' => 'LogController@lumen']);

$router->get('matchList/{cryptoShortName}/session/{sessionId}', [
'as' => 'display', 'uses' => 'MatchListController@display']);
